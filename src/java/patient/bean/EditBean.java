/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patient.bean;

import patient.bean.*;
import java.util.Date;

/**
 *
 * @author root
 */
public class EditBean 
{

    private String FIO;
    private Date date_birth;
    private int hidden_id, assigned_therapist_id;
    
    public String getFIO(){
        return FIO;
    }
    public void setFIO(String FIO){
        this.FIO=FIO;
    }
    public int getAssigned_therapist(){
        return assigned_therapist_id;
    }
    public void setAssigned_therapist(int assigned_therapist_id){
        this.assigned_therapist_id=assigned_therapist_id;
    }
    public Date getDate_birth(){
        return date_birth;
    }
    public void setDate_birth(Date date_birth){
        this.date_birth=date_birth;
    }
    public int getHidden_ID(){
        return hidden_id;
    }
    public void setHidden_ID(int hidden_id){
        this.hidden_id=hidden_id;
    }
            
}
