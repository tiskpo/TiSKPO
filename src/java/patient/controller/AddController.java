/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patient.controller;

import patient.controller.*;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patient.bean.AddBean;
import patient.dao.AddDao;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author root
 */
public class AddController extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        if(request.getParameter("btn_add")!=null) //check button click event not null from add.jsp page after continue
        {
            String FIO=request.getParameter("txt_FIO"); 
            String regex_FIO = "^([а-яА-Я]+){1}( [а-яА-Я]+){1,2}$";
            Pattern pattern_FIO = Pattern.compile(regex_FIO);
            Matcher matcher_FIO = pattern_FIO.matcher(FIO);
            
            
            String date_birth_str = request.getParameter("date_date_birth");
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            try {
                Date date_birth = formatter.parse(date_birth_str);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date_birth);
                int date_birth_year = calendar.get(Calendar.YEAR);
                int year = Calendar.getInstance().get(Calendar.YEAR);
            
                if(!matcher_FIO.matches()){
                    request.setAttribute("InsertErrorMsg","Не корректное фио");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else if(request.getParameter("assigned_therapist") == null){
                    request.setAttribute("InsertErrorMsg","Не указан врач");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else{
                    int assigned_therapist=Integer.parseInt(request.getParameter("assigned_therapist"));
            
                    AddBean addBean=new AddBean(); //this class contain setting up all receive values from add.jsp page to seeter and getter method for application require effectively
            
                    addBean.setFIO(FIO); //set name through addBean object
                    addBean.setAssigned_therapist(assigned_therapist); //set owner through addBean object
                    addBean.setDate_birth(date_birth);
            
                    AddDao addDao=new AddDao(); //this class contain main logic to perform function calling and database operation
            
                    String insertValidate=addDao.checkInsert(addBean); //send addBean object values into checkInsert() function in AddDao class
            
                    if(insertValidate.equals("INSERT SUCCESS")) //check calling checkInsert() function receive string "INSERT SUCCESS" after redirect to index.jsp page and display record
                    {
                        request.setAttribute("InsertSuccessMsg",insertValidate); //setAttribute value is "InsertSuccessMsg" for insert successfully message
                        RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
                        rd.forward(request, response);
                    }
                    else
                    {
                        request.setAttribute("InsertErrorMsg",insertValidate); //setAttribute value is "InsertErrorMsg" for insert fail message
                        RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                        rd.include(request, response);
                    }
                }
            }
            catch (ParseException e) {
                request.setAttribute("InsertErrorMsg","Не корректная дата");
                RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                rd.include(request, response);
            }
        }
    }
}
