/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patient.controller;
import patient.controller.*;
import patient.bean.AddBean;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patient.bean.EditBean;
import patient.dao.AddDao;
import patient.dao.EditDao;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author root
 */
public class EditController extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        if(request.getParameter("btn_edit")!=null) //check button click event not null from add.jsp page after continue
        {
            String FIO=request.getParameter("txt_FIO"); 
            String regex_FIO = "^([а-яА-Я]+){1}( [а-яА-Я]+){1,2}$";
            Pattern pattern_FIO = Pattern.compile(regex_FIO);
            Matcher matcher_FIO = pattern_FIO.matcher(FIO);
            
            
            String date_birth_str = request.getParameter("date_date_birth");
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            try {
                Date date_birth = formatter.parse(date_birth_str);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date_birth);
                int date_birth_year = calendar.get(Calendar.YEAR);
                int year = Calendar.getInstance().get(Calendar.YEAR);
            
                if(!matcher_FIO.matches()){
                    request.setAttribute("InsertErrorMsg","Не корректное фио");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else if(request.getParameter("assigned_therapist") == null){
                    request.setAttribute("InsertErrorMsg","Не указан врач");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else{
                    int assigned_therapist=Integer.parseInt(request.getParameter("assigned_therapist"));
                    int hidden_id=Integer.parseInt(request.getParameter("hidden_id"));
                    EditBean editBean=new EditBean(); //this class contain setting up all receive values from add.jsp page to seeter and getter method for application require effectively
            
                    editBean.setFIO(FIO); //set name through addBean object
                    editBean.setAssigned_therapist(assigned_therapist); //set owner through addBean object
                    editBean.setDate_birth(date_birth);
                    editBean.setHidden_ID(hidden_id);
            
                    EditDao editDao=new EditDao(); //this class contain main logic to perform function calling and database operation
            
                    String updateValidate=editDao.checkUpdate(editBean); //send editBean object values into checkUpdate() function in EditDao class
            
                    if(updateValidate.equals("UPDATE SUCCESS")) //check calling checkUpdate() function receive string "UPDATE SUCCESS" after redirect to index.jsp page and display update record
                    {
                        request.setAttribute("UpdateSuccessMsg",updateValidate); //setAttribute value is "UpdateSuccessMsg" for update successfully message
                        RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
                        rd.forward(request, response);
                    }
                    else
                    {
                        request.setAttribute("UpdateErrorMsg",updateValidate); //setAttribute value is "UpdateErrorMsg" for update fail message
                        RequestDispatcher rd=request.getRequestDispatcher("edit.jsp");
                        rd.include(request, response);
                    }
                }
            }
            catch (ParseException e) {
                request.setAttribute("InsertErrorMsg","Не корректная дата");
                RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                rd.include(request, response);
            }
        }
    }
}
