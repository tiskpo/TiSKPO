/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment.dao;

import appointment.dao.*;
import appointment.dao.*;
import appointment.bean.AddBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
/**
 *
 * @author root
 */
public class AddDao 
{
    public String checkInsert(AddBean addBean)
    {
        int id_doctor=addBean.getID_doctor();
        int id_patient=addBean.getID_patient();
        Date date_appointment=addBean.getDate_appointment(); 
        Time time_appointment=addBean.getTime_appointment(); 
        
        String url="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
        String username="root"; //database connection username
        String password="root"; //database password
        
        try
        {
            Class.forName("com.mysql.jdbc.Driver"); //load driver
            Connection con=DriverManager.getConnection(url,username,password); //create connection
            
            PreparedStatement pstmt=null; //create statement
            
            pstmt=con.prepareStatement("insert into appointment(id_doctor,id_patient,date_appointment,time_appointment) values(?,?,?,?)"); //sql insert query
            pstmt.setInt(1,id_doctor);
            pstmt.setInt(2,id_patient);
            pstmt.setDate(3, new java.sql.Date(date_appointment.getTime()));
            pstmt.setTime(4, time_appointment);
            pstmt.executeUpdate(); //execute query
            
            pstmt.close(); //close statement
            
            con.close(); //close connection
            
            return "INSERT SUCCESS"; //if valid return "INSERT SUCCESS" string
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return "FAIL INSERT"; //if invalid return "FAIL INSERT" string
    }
    
}