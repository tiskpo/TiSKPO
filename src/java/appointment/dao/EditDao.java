/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment.dao;
import appointment.dao.*;
import appointment.dao.*;
import appointment.bean.AddBean;
import appointment.bean.EditBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author root
 */
public class EditDao {
    public String checkUpdate(EditBean editBean)
    {
        int id_doctor=editBean.getID_doctor();
        int id_patient=editBean.getID_patient();
        Date date_appointment=editBean.getDate_appointment(); 
        Time time_appointment=editBean.getTime_appointment(); 
        int hidden_id=editBean.getHidden_ID();
        
        String url="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
        String username="root"; //database connection username
        String password="root"; //database password
        
        try
        {
            Class.forName("com.mysql.jdbc.Driver"); //load driver
            Connection con=DriverManager.getConnection(url,username,password); //create connection
            
            PreparedStatement pstmt=null; //create statement
            
            pstmt=con.prepareStatement("update appointment set id_doctor=?,id_patient=?,date_appointment=?,time_appointment=? where id=? "); //sql insert query
            pstmt.setInt(1,id_doctor);
            pstmt.setInt(2,id_patient);
            pstmt.setDate(3, new java.sql.Date(date_appointment.getTime()));
            pstmt.setTime(4, time_appointment);
            pstmt.setInt(5,hidden_id);
            pstmt.executeUpdate(); //execute query
            
            pstmt.close(); //close statement
            
            con.close(); //close connection
            
            return "UPDATE SUCCESS"; //if valid return "INSERT SUCCESS" string
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return "FAIL UPDATE"; //if invalid return "FAIL INSERT" string
    }
    
}
