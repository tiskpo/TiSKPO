/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment.bean;

import appointment.bean.*;
import appointment.bean.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author root
 */
public class AddBean {

    private Time time_time_appointment;
    
    private Date date_date_appointment;
    private int id_doctor, id_patient;
    
    public int getID_doctor(){
        return id_doctor;
    }
    public void setID_doctor(int id_doctor){
        this.id_doctor=id_doctor;
    }
    public int getID_patient(){
        return id_patient;
    }
    public void setID_patient(int id_patient){
        this.id_patient=id_patient;
    }
    public Date getDate_appointment(){
        return date_date_appointment;
    }
    public void setDate_appointment(Date date_date_appointment){
        this.date_date_appointment=date_date_appointment;
    }
    public Time getTime_appointment(){
        return time_time_appointment;
    }
    public void setTime_appointment(Time time_time_appointment){
        this.time_time_appointment=time_time_appointment;
    }
}
