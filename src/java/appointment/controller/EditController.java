/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment.controller;
import appointment.bean.AddBean;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import appointment.bean.EditBean;
import appointment.dao.AddDao;
import java.sql.Timestamp;
import appointment.dao.EditDao;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author root
 */
public class EditController extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        if(request.getParameter("btn_edit")!=null) //check button click event not null from add.jsp page after continue
        {
            
            String date_appointment_str = request.getParameter("date_date_appointment");
            SimpleDateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            
            String time_appointment_str = request.getParameter("time_time_appointment");
            SimpleDateFormat formatter_time = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            try {
                int hidden_id=Integer.parseInt(request.getParameter("hidden_id"));
                
                Date date_appointment = formatter_date.parse(date_appointment_str);
                Time time_appointment = new Time(formatter_time.parse(time_appointment_str).getTime());
            
                if(request.getParameter("id_patient") == null){
                    request.setAttribute("InsertErrorMsg","Не указан пациент");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else if(request.getParameter("id_doctor") == null){
                    request.setAttribute("InsertErrorMsg","Не указан врач");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else{

                    int id_doctor=Integer.parseInt(request.getParameter("id_doctor"));

                    int id_patient=Integer.parseInt(request.getParameter("id_patient"));

                    EditBean editBean=new EditBean(); //this class contain setting up all receive values from add.jsp page to seeter and getter method for application require effectively

                    editBean.setID_doctor(id_doctor); //set name through addBean object
                    editBean.setID_patient(id_patient); //set owner through addBean object
                    editBean.setDate_appointment(date_appointment);
                    editBean.setTime_appointment(time_appointment);
                    editBean.setHidden_ID(hidden_id);

                    EditDao editDao=new EditDao(); //this class contain main logic to perform function calling and database operation

                    String updateValidate=editDao.checkUpdate(editBean); //send editBean object values into checkUpdate() function in EditDao class

                    if(updateValidate.equals("UPDATE SUCCESS")) //check calling checkUpdate() function receive string "UPDATE SUCCESS" after redirect to index.jsp page and display update record
                    {
                        request.setAttribute("UpdateSuccessMsg",updateValidate); //setAttribute value is "UpdateSuccessMsg" for update successfully message
                        RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
                        rd.forward(request, response);
                    }
                    else
                    {
                        request.setAttribute("UpdateErrorMsg",updateValidate); //setAttribute value is "UpdateErrorMsg" for update fail message
                        RequestDispatcher rd=request.getRequestDispatcher("edit.jsp");
                        rd.include(request, response);
                    }
                }
            }
            catch (ParseException e) {
                request.setAttribute("InsertErrorMsg","Не корректная дата или время");
                RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                rd.include(request, response);
            }
        }
    }

}
