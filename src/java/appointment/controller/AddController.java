/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appointment.controller;

import appointment.controller.*;
import appointment.controller.*;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import appointment.bean.AddBean;
import appointment.dao.AddDao;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author root
 */
public class AddController extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        if(request.getParameter("btn_add")!=null) //check button click event not null from add.jsp page after continue
        {
            
            String date_appointment_str = request.getParameter("date_date_appointment");
            SimpleDateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            
            String time_appointment_str = request.getParameter("time_time_appointment");
            SimpleDateFormat formatter_time = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            try {
                
                Date date_appointment = formatter_date.parse(date_appointment_str);
                Time time_appointment = new Time(formatter_time.parse(time_appointment_str).getTime());
            
                if(request.getParameter("id_patient") == null){
                    request.setAttribute("InsertErrorMsg","Не указан пациент");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else if(request.getParameter("id_doctor") == null){
                    request.setAttribute("InsertErrorMsg","Не указан врач");
                    RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                    rd.include(request, response);
                } else{

                    int id_doctor=Integer.parseInt(request.getParameter("id_doctor"));

                    int id_patient=Integer.parseInt(request.getParameter("id_patient"));

                    AddBean addBean=new AddBean(); //this class contain setting up all receive values from add.jsp page to seeter and getter method for application require effectively

                    addBean.setID_doctor(id_doctor); //set name through addBean object
                    addBean.setID_patient(id_patient); //set owner through addBean object
                    addBean.setDate_appointment(date_appointment);
                    addBean.setTime_appointment(time_appointment);

                    AddDao addDao=new AddDao(); //this class contain main logic to perform function calling and database operation

                    String insertValidate=addDao.checkInsert(addBean); //send addBean object values into checkInsert() function in AddDao class

                    if(insertValidate.equals("INSERT SUCCESS")) //check calling checkInsert() function receive string "INSERT SUCCESS" after redirect to index.jsp page and display record
                    {
                        request.setAttribute("InsertSuccessMsg",insertValidate); //setAttribute value is "InsertSuccessMsg" for insert successfully message
                        RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
                        rd.forward(request, response);
                    }
                    else
                    {
                        request.setAttribute("InsertErrorMsg",insertValidate); //setAttribute value is "InsertErrorMsg" for insert fail message
                        RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                        rd.include(request, response);
                    }
                }
            }
            catch (ParseException e) {
                request.setAttribute("InsertErrorMsg","Не корректная дата");
                RequestDispatcher rd=request.getRequestDispatcher("add.jsp");
                rd.include(request, response);
            }
        }
    }
}
