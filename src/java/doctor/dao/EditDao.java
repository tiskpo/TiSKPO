/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctor.dao;
import doctor.bean.AddBean;
import doctor.bean.EditBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Date;

/**
 *
 * @author root
 */
public class EditDao {
    public String checkUpdate(EditBean editBean)
    {
        String FIO=editBean.getFIO(); 
        String specialization=editBean.getSpecialization();
        Date date_birth=editBean.getDate_birth(); 
        int hidden_id=editBean.getHidden_ID();
        
        String url="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
        String username="root"; //database connection username
        String password="root"; //database password
        
        try
        {
            Class.forName("com.mysql.jdbc.Driver"); //load driver
            Connection con=DriverManager.getConnection(url,username,password); //create connection
            
            PreparedStatement pstmt=null; //create statement
            
            pstmt=con.prepareStatement("update doctor set FIO=?,specialization=?,date_birth=? where id=? "); //sql insert query
            pstmt.setString(1,FIO);
            pstmt.setString(2,specialization);
            pstmt.setDate(3, new java.sql.Date(date_birth.getTime()));
            pstmt.setInt(4,hidden_id);
            pstmt.executeUpdate(); //execute query
            
            pstmt.close(); //close statement
            
            con.close(); //close connection
            
            return "UPDATE SUCCESS"; //if valid return "INSERT SUCCESS" string
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return "FAIL UPDATE"; //if invalid return "FAIL INSERT" string
    }
    
}
