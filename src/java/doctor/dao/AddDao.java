/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctor.dao;

import doctor.bean.AddBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;
/**
 *
 * @author root
 */
public class AddDao 
{
    public String checkInsert(AddBean addBean)
    {
        String FIO=addBean.getFIO(); 
        String specialization=addBean.getSpecialization();
        Date date_birth=addBean.getDate_birth(); 
        
        String url="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
        String username="root"; //database connection username
        String password="root"; //database password
        
        try
        {
            Class.forName("com.mysql.jdbc.Driver"); //load driver
            Connection con=DriverManager.getConnection(url,username,password); //create connection
            
            PreparedStatement pstmt=null; //create statement
            
            pstmt=con.prepareStatement("insert into doctor(FIO,specialization,date_birth) values(?,?,?)"); //sql insert query
            pstmt.setString(1,FIO);
            pstmt.setString(2,specialization);
            pstmt.setDate(3, new java.sql.Date(date_birth.getTime()));
            pstmt.executeUpdate(); //execute query
            
            pstmt.close(); //close statement
            
            con.close(); //close connection
            
            return "INSERT SUCCESS"; //if valid return "INSERT SUCCESS" string
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return "FAIL INSERT"; //if invalid return "FAIL INSERT" string
    }
    
}