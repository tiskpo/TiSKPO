/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doctor.bean;

import java.util.Date;

/**
 *
 * @author root
 */
public class EditBean 
{

    private String FIO,specialization;
    private Date date_birth;
    private int hidden_id;
    
    public String getFIO(){
        return FIO;
    }
    public void setFIO(String FIO){
        this.FIO=FIO;
    }
    public String getSpecialization(){
        return specialization;
    }
    public void setSpecialization(String specialization){
        this.specialization=specialization;
    }
    public Date getDate_birth(){
        return date_birth;
    }
    public void setDate_birth(Date date_birth){
        this.date_birth=date_birth;
    }
    public int getHidden_ID(){
        return hidden_id;
    }
    public void setHidden_ID(int hidden_id){
        this.hidden_id=hidden_id;
    }
            
}
