<%-- 
    Document   : newjsp
    Created on : 27.10.2021, 14:43:16
    Author     : root
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java MVC Add, Edit, Delete Using JSP & Servlet With MySQL</title>
        <link href="../css/main.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        
        <div class="main">	
            <center>
		<h1><a class="button" href="add.jsp">Добавить запись</a></h1>	
            </center>
		
            <table class="table">    
                
		<tr>
                    <th>ID</th>
                    <th>ID доктора</th>
                    <th>ID пациента</th>
                    <th>Дата записи</th>
                    <th>Врямя записи</th>
                    <th>Изменить</th>
                    <th>Удалить</th>
                </tr>
                <%
                    String url="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
                    String username="root"; //database connection username
                    String password="root"; //database password
                  
                try
                {
                    Class.forName("com.mysql.jdbc.Driver"); //load driver
                    Connection con=DriverManager.getConnection(url,username,password); //create connection
                  
                    PreparedStatement pstmt=null; //create statement
                  
                    pstmt=con.prepareStatement("select * from appointment"); //sql select query
                    ResultSet rs=pstmt.executeQuery(); //execute query and set in ResultSet object rs.
                    
                    while(rs.next())
                    {
                %>
		<tr>
                    <td><%=rs.getInt("id")%></td>
                    <td><%=rs.getInt("id_doctor")%></td>
                    <td><%=rs.getInt("id_patient")%></td>
                    <td><%=rs.getDate("date_appointment")%></td>
                    <td><%=rs.getTime("time_appointment")%></td>
                    
                    <td><a class="button" href="edit.jsp?edit_id=<%=rs.getInt("id")%>">Изменить</a></td>
                    <td><a class="button" href="delete.jsp?delete_id=<%=rs.getInt("id")%>">Удалить</a></td>
                    
		</tr>
                <%
                    }
                    
                    pstmt.close(); //close statement

                    con.close(); //close connection
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
                %>
            </table>
		
            <center>
                <h3 style="color:green;">
                <%
                    if(request.getAttribute("InsertSuccessMsg")!=null)
                    {
                        out.println(request.getAttribute("InsertSuccessMsg")); //get record insert success message from AddController.java
                    }
                %>
                
                <%
                    if(request.getAttribute("UpdateSuccessMsg")!=null)
                    {
                        out.println(request.getAttribute("UpdateSuccessMsg")); //get record update success message from EditController.java
                    }
                %>
                </h3>
                <h3><a class="button" href="../index.jsp">Назад</a></h3>
            </center>
            
	</div>
		
    </body>
   
</html>