<%-- 
    Document   : edit
    Created on : 27.10.2021, 17:34:37
    Author     : root
--%>

<%@page import="java.util.Objects"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java MVC Add, Edit, Delete Using JSP & Servlet With MySQL</title>
        <link href="../css/main.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main">
            <center>
            	<h1>Изменить запись</h1>
            </center>
		
        <form method="post" action="EditController">
                
            <table class="table">	        
            <%
            if(request.getParameter("edit_id")!=null) //get edit_id from index.jsp page with href link and check not null after continue
            {
                int id=Integer.parseInt(request.getParameter("edit_id")); //get edit_id store in "id" variable
                
                String url="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
                String username="root"; //database connection username
                String password="root"; //database password
                
                try
                {
                    Class.forName("com.mysql.jdbc.Driver"); //load driver
                    Connection con=DriverManager.getConnection(url,username,password); //create connection
                    
                    PreparedStatement pstmt=null; //create statement
                    
                    pstmt=con.prepareStatement("select * from appointment where id=?"); //sql select query 
                    pstmt.setInt(1,id);
                    ResultSet rs=pstmt.executeQuery(); //execute query and set in Resultset object rs.
                    
                    while(rs.next())
                    {
            %>    
                
                <tr>
                    <td>Доктор</td>
                    <td>
                    <select size="5" name="id_doctor">
                    <%pstmt=con.prepareStatement("select * from doctor"); //sql select query 
                    ResultSet rs_ad=pstmt.executeQuery(); 
                    
                    while(rs_ad.next())
                    {
                    %> 
                    <% if(java.util.Objects.equals(rs_ad.getInt("id"), rs.getInt("id_doctor"))){%>
                    <option selected value=<%=rs_ad.getInt("id")%>><%=rs_ad.getString("FIO")%></option> 

                    <%} 
                    else{%>
                    <option value=<%=rs_ad.getInt("id")%>><%=rs_ad.getString("FIO")%></option> 
                    
                    <%}
                    }%>
                    </select>
                    </td>
                </tr>
                
                <tr>
                    <td>Пациент</td>
                    <td>
                    <select size="5" name="id_patient">
                    <%pstmt=con.prepareStatement("select * from patient"); //sql select query 
                    ResultSet rs_ap=pstmt.executeQuery(); 
                    
                    while(rs_ap.next())
                    {
                    %> 
                    <% if(java.util.Objects.equals(rs_ap.getInt("id"), rs.getInt("id_patient"))){%>
                    <option selected value=<%=rs_ap.getInt("id")%>><%=rs_ap.getString("FIO")%></option> 

                    <%} 
                    else{%>
                    <option value=<%=rs_ap.getInt("id")%>><%=rs_ap.getString("FIO")%></option> 
                    
                    <%}
                    }%>
                    </select>
                    </td>
                </tr>
				
                <tr>
                    <td>Дата записи</td>
                    <td><input type="date" name="date_date_appointment" id="date_appointment" value="<%=rs.getDate("date_appointment")%>"></td>
                </tr>	
				
                <tr>
                    <td>Время записи</td>
                    <td><input type="time" name="time_time_appointment" id="time_appointment" value="<%=rs.getTime("time_appointment")%>"></td>
                </tr>	
				
                <tr>
                    <td><input type="submit" name="btn_edit" value="Изменить"></td>	
                </tr>
                
                    <input type="hidden" name="hidden_id" value="<%=rs.getInt("id")%>">
            <%
                    }
                    
                    pstmt.close(); //close statement

                    con.close(); //close connection
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            %>
            </table>
             
            <center>
                <h3 style="color:red;">
                    <%
                        if(request.getAttribute("UpdateErrorMsg")!=null)
                        {
                            out.print(request.getAttribute("UpdateErrorMsg")); //get update record fail error message from EditController.java
                        }
                    %>
                </h3>
                
                <h3><a class="button" href="index.jsp">Назад</a></h3>
            </center>
				
        </form>
		
	</div>
	

    </body>
</html>

