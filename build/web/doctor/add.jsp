<%-- 
    Document   : add
    Created on : 27.10.2021, 11:06:13
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java MVC Add, Edit, Delete Using JSP & Servlet With MySQL</title>
        <link href="../css/main.css" rel="stylesheet" type="text/css">
        	

    </head>
    <body>
        <div class="main">
            <center>
            	<h1>Добавить врача</h1>
            </center>
            
        <form method="post" action="AddController">
                
            <table class="table">	        
                
                <tr>
                    <td>ФИО</td>
                    <td><input type="text" name="txt_FIO" id="FIO"></td>
                </tr>
				
                <tr>
                    <td>Дата рождения</td>
                    <td><input type="date" name="date_date_birth" id="date_birth"></td>
                </tr>	
				
                <tr>
                    <td>Специализация</td>
                    <td><input type="text" name="txt_specialization" id="specialization"></td>
                </tr>	
				
                <tr>
                    <td><input type="submit" name="btn_add" value="Добавить"></td>	
                </tr>
				
            </table>
               
            <center>
                <h3 style="color:red;">
                <%
                    if(request.getAttribute("InsertErrorMsg")!=null)
                    {
                        out.println(request.getAttribute("InsertErrorMsg")); //get insert record fail error message from AddController.java
                    }
                %>
                </h3>
             
                <h3><a class="button" href="index.jsp">Назад</a></h3>
                
            </center>
				
        </form>
		
	</div>
       
    </body>
</html>
