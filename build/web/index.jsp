<%-- 
    Document   : index
    Created on : 28.10.2021, 12:50:51
    Author     : root
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java MVC Add, Edit, Delete Using JSP & Servlet With MySQL</title>
        <link href="css/main.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        
        <div class="main">
            <center>
		<h1>Сегодняшние запись к врачам</h1>	
            </center>
            
            <center>
            <table class="table">    
                
		<tr>
                    <th>ФИО врача</th>
                    <th>ФИО больного</th>
                    <th>Дата</th>
                    <th>Время</th>
                </tr>
                <%
                    String url_f="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
                    String username_f="root"; //database connection username
                    String password_f="root"; //database password
                  
                try
                {
                    Class.forName("com.mysql.jdbc.Driver"); //load driver
                    Connection con_f=DriverManager.getConnection(url_f,username_f,password_f); //create connection
                  
                    PreparedStatement pstmt_f=null; //create statement
                    
                    
                    pstmt_f=con_f.prepareStatement("select * from appointment WHERE date_appointment=CURRENT_DATE() ORDER BY time_appointment"); //sql select query
                    
                    ResultSet rs_f=pstmt_f.executeQuery(); //execute query and set in ResultSet object rs.
                    
                    while(rs_f.next())
                    {
                %>
		<tr>
                    <%pstmt_f=con_f.prepareStatement("select * from doctor where id=?"); //sql select query 
                    pstmt_f.setInt(1,rs_f.getInt("id_doctor"));
                    ResultSet rs_d_f=pstmt_f.executeQuery();
                    rs_d_f.next();
                    %> 
                    <td><%=rs_d_f.getString("FIO")%></td>
                    <%pstmt_f=con_f.prepareStatement("select * from patient where id=?"); //sql select query 
                    pstmt_f.setInt(1,rs_f.getInt("id_patient"));
                    ResultSet rs_p_f=pstmt_f.executeQuery();
                    rs_p_f.next();
                    %> 
                    <td><%=rs_p_f.getString("FIO")%></td>
                    <td><%=rs_f.getDate("date_appointment")%></td>
                    <td><%=rs_f.getTime("time_appointment")%></td>
                    
		</tr>
                <%
                    }
                    
                    pstmt_f.close(); //close statement

                    con_f.close(); //close connection
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
                %>
            </table>
                </h3>
                <h3><a class="button" href="doctor/index.jsp">Доктора</a></h3>
                <h3><a class="button" href="patient/index.jsp">Пациенты</a></h3>
                <h3><a class="button" href="appointment/index.jsp">Записи к врачу</a></h3>
            </center>
            
	</div>
		
    </body>
   
</html>
