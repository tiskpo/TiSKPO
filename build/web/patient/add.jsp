<%-- 
    Document   : add
    Created on : 27.10.2021, 11:06:13
    Author     : root
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java MVC Add, Edit, Delete Using JSP & Servlet With MySQL</title>
        <link href="../css/main.css" rel="stylesheet" type="text/css">
        	

    </head>
    <body>
        <div class="main">
            <center>
            	<h1>Добавить пациента</h1>
            </center>
            
        <form method="post" action="AddController">
                
            <table class="table">	        
                
                <tr>
                    <td>ФИО</td>
                    <td><input type="text" name="txt_FIO" id="FIO"></td>
                </tr>
				
                <tr>
                    <td>Дата рождения</td>
                    <td><input type="date" name="date_date_birth" id="date_birth"></td>
                </tr>	
				
                <tr>
                    <td>Приписанный терапевт</td>
                    <td>
                    <select size="5" name="assigned_therapist">
                    <%
                    String url="jdbc:mysql://localhost:3306/testBlog"; //database connection url string
                    String username="root"; //database connection username
                    String password="root"; //database password
                
                    try
                    {
                        Class.forName("com.mysql.jdbc.Driver"); //load driver
                        Connection con=DriverManager.getConnection(url,username,password); //create connection
                    
                        PreparedStatement pstmt=null; //create statement
                        pstmt=con.prepareStatement("select * from doctor"); //sql select query 
                        ResultSet rs_ad=pstmt.executeQuery(); 
                    
                        while(rs_ad.next())
                        {
                    %>   
                    <option value=<%=rs_ad.getInt("id")%>><%=rs_ad.getString("FIO")%></option>
                    <%
                    
                        }
                        pstmt.close(); //close statement

                        con.close(); //close connection
                        }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                        }
                    %>
                    </select>
                    </td>
                </tr>
				
                <tr>
                    <td><input type="submit" name="btn_add" value="Добавить"></td>	
                </tr>
				
            </table>
               
            <center>
                <h3 style="color:red;">
                <%
                    if(request.getAttribute("InsertErrorMsg")!=null)
                    {
                        out.println(request.getAttribute("InsertErrorMsg")); //get insert record fail error message from AddController.java
                    }
                %>
                </h3>
             
                <h3><a class="button" href="index.jsp">Назад</a></h3>
                
            </center>
				
        </form>
		
	</div>
       
    </body>
</html>
